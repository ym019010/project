#include "common.h"
#include "time.h"
#include "stdint.h"
#include "conio.h"


const time_t total_time = 100;
char name;
void Time_Counter()
{
	timer.time_taken = SDL_GetTicks() / 1000;
	timer.time_remaining = total_time - timer.time_taken;
}

void initTimer(void)
{

	memset(&timer, 0, sizeof(Timer));

	Time_Counter();


}
void Ender()
{
	if (timer.time_remaining == 0)
	{
		SDL_Delay(100);
		exit(1);
	}
}
